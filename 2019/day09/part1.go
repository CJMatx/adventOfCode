package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

const (
	position  = 0
	immediate = 1
	relative  = 2

	add      = 1
	mul      = 2
	in       = 3
	out      = 4
	jmpTrue  = 5
	jmpFalse = 6
	less     = 7
	equals   = 8
	adjust   = 9
	halt     = 99

	extraMemory = 1000
)

type Computer struct {
	instPtr      int
	relativeBase int
	inputChan    chan int
	memory       []int
	done         chan bool
}

func (c *Computer) add(val1, val2, outputPosition int) {
	c.memory[outputPosition] = c.memory[val1] + c.memory[val2]
	c.instPtr += 4
}

func (c *Computer) mul(val1, val2, outputPosition int) {
	c.memory[outputPosition] = c.memory[val1] * c.memory[val2]
	c.instPtr += 4
}

func (c *Computer) input(position int) {
	c.memory[position] = <-c.inputChan
	c.instPtr += 2
}

func (c *Computer) output(position int) {
	fmt.Println(c.memory[position])
	c.instPtr += 2
}

func (c *Computer) jumpIfTrue(val, position int) {
	if c.memory[val] != 0 {
		c.instPtr = c.memory[position]
	} else {
		c.instPtr += 3
	}
}

func (c *Computer) jumpIfFalse(val, position int) {
	if c.memory[val] == 0 {
		c.instPtr = c.memory[position]
	} else {
		c.instPtr += 3
	}
}

func (c *Computer) lessThan(val1, val2, position int) {
	if c.memory[val1] < c.memory[val2] {
		c.memory[position] = 1
	} else {
		c.memory[position] = 0
	}
	c.instPtr += 4
}

func (c *Computer) equals(val1, val2, position int) {
	if c.memory[val1] == c.memory[val2] {
		c.memory[position] = 1
	} else {
		c.memory[position] = 0
	}
	c.instPtr += 4
}

func (c *Computer) adjustRelativeBase(val int) {
	c.relativeBase += c.memory[val]
	c.instPtr += 2
}

func (c *Computer) processInstruction() (op int, params []int) {
	instruction := c.memory[c.instPtr]
	op = instruction % 100
	if op == halt {
		return op, params
	}
	numParams := 3
	if op == in || op == out || op == adjust {
		numParams = 1
	} else if op == jmpTrue || op == jmpFalse {
		numParams = 2
	}
	params = make([]int, numParams)

	for i := 0; i < numParams; i++ {
		switch instruction / int(math.Pow10(i+2)) % 10 {
		case position:
			params[i] = c.memory[c.instPtr+i+1]
		case immediate:
			params[i] = c.instPtr + i + 1
		case relative:
			params[i] = c.memory[c.instPtr+i+1] + c.relativeBase
		}
	}
	return op, params
}

func (c *Computer) run() {
	running := true
	for running {
		op, params := c.processInstruction()
		switch op {
		case add:
			c.add(params[0], params[1], params[2])
		case mul:
			c.mul(params[0], params[1], params[2])
		case in:
			c.input(params[0])
		case out:
			c.output(params[0])
		case jmpTrue:
			c.jumpIfTrue(params[0], params[1])
		case jmpFalse:
			c.jumpIfFalse(params[0], params[1])
		case less:
			c.lessThan(params[0], params[1], params[2])
		case equals:
			c.equals(params[0], params[1], params[2])
		case adjust:
			c.adjustRelativeBase(params[0])
		case halt:
			running = false
			c.done <- true
		default:
			log.Fatalf("Something went wrong! Unknown OpCode - op: %d, params: %v, instPtr: %d", op, params, c.instPtr)
		}
	}
}

func main() {
	codeAsString, err := readLine("2019/day09/input.txt")
	if err != nil {
		log.Fatal(err.Error())
	}

	input := make(chan int, 1)
	input <- 1
	boost := newComputer(codeAsString, input)
	fmt.Println("Part1:")
	go boost.run()
	<-boost.done

	input = make(chan int, 1)
	input <- 2
	boost = newComputer(codeAsString, input)
	fmt.Println("Part2:")
	go boost.run()
	<-boost.done
}

func newComputer(codeAsString string, input chan int) Computer {
	splitCode := strings.Split(codeAsString, ",")
	computer := Computer{
		instPtr:      0,
		relativeBase: 0,
		memory:       make([]int, len(splitCode)+extraMemory),
		inputChan:    input,
		done:         make(chan bool),
	}

	for i, val := range splitCode {
		code, err := strconv.Atoi(val)
		if err != nil {
			log.Fatal(err.Error())
		}
		computer.memory[i] = code
	}

	return computer
}

func readLine(path string) (string, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	split := strings.Split(string(bytes), "\n")

	return split[0], nil
}
