package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	lines, err := readLines("input.txt")
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	totalFuel := 0
	for _, line := range lines {
		if line == "" {
			continue
		}
		mass, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		totalFuel += calcReqFuel(mass)
	}

	fmt.Printf("Required fuel: %d", totalFuel)
}

func calcReqFuel(mass int) int {
	return (mass / 3) - 2
}

// readLines creates a []string with each entry being a line from the file 'path'
func readLines(path string) ([]string, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return strings.Split(string(bytes), "\n"), nil
}
