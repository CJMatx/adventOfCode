package main

import (
	"fmt"
	"math"
)

const min = 273025
const max = 767253

func main() {
	totalMatches := 0

OUTER:
	for i := min; i <= max; i++ {
		prevInt := i / 100000
		intCount := map[int]int{prevInt: 1}
		for j := 1; j < 6; j++ {
			currInt := i / (100000 / int(math.Pow10(j))) % 10
			if prevInt > currInt {
				continue OUTER
			}
			if count, ok := intCount[currInt]; ok {
				intCount[currInt] = count + 1
			} else {
				intCount[currInt] = 1
			}
			prevInt = currInt
		}

		for _, v := range intCount {
			if v == 2 {
				totalMatches++
				break
			}
		}
	}

	fmt.Printf("Answer: %d\n", totalMatches)
}
