package main

import (
	"fmt"
	"math"
)

const min = 273025
const max = 767253

func main() {
	totalMatches := 0

OUTER:
	for i := min; i <= max; i++ {
		match := false
		prevInt := i / 100000
		for j := 1; j < 6; j++ {
			currInt := i / (100000 / int(math.Pow10(j))) % 10
			if prevInt > currInt {
				continue OUTER
			} else if prevInt == currInt {
				match = true
			}
			prevInt = currInt
		}

		if match {
			totalMatches++
		}
	}

	fmt.Printf("Answer: %d\n", totalMatches)
}
