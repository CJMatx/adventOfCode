package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	lines, err := readLines("input.txt")
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	intCode := []int{}
	for _, codeString := range strings.Split(lines[0], ",") {
		code, err := strconv.Atoi(codeString)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		intCode = append(intCode, code)
	}

	part1 := setupIntCode(intCode, 12, 2)
	fmt.Printf("Part 1 Answer: %d\n", part1)

	for n := 0; n < 100; n++ {
		for v := 0; v < 100; v++ {
			if setupIntCode(intCode, n, v) == 19690720 {
				fmt.Printf("Part 2 Answer: %d\n", 100*n+v)
				return
			}
		}
	}
}

func setupIntCode(intCode []int, noun int, verb int) int {
	intCodeCopy := make([]int, len(intCode))
	copy(intCodeCopy, intCode)
	intCodeCopy[1] = noun
	intCodeCopy[2] = verb
	processIntCode(intCodeCopy, 0)
	return intCodeCopy[0]
}

func processIntCode(intCode []int, position int) {
	switch intCode[position] {
	case 1:
		firstPos := intCode[position+1]
		secondPos := intCode[position+2]
		answerPosition := intCode[position+3]
		intCode[answerPosition] = intCode[firstPos] + intCode[secondPos]
	case 2:
		firstPos := intCode[position+1]
		secondPos := intCode[position+2]
		answerPosition := intCode[position+3]
		intCode[answerPosition] = intCode[firstPos] * intCode[secondPos]
	case 99:
		return
	default:
		fmt.Printf("Something went wrong! OpCode: %d", intCode[position])
		return
	}
	processIntCode(intCode, position+4)
}

// readLines creates a []string with each entry being a line from the file 'path'
func readLines(path string) ([]string, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	split := strings.Split(string(bytes), "\n")
	split = split[:len(split)-1]

	return split, nil
}
