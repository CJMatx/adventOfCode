package main

import (
	"fmt"
	"io/ioutil"
)

const (
	width  = 25
	height = 6

	black = '0'
	white = '1'
)

func main() {
	layers := parseFile("2019/day08/input.txt")
	fmt.Println("Part1:", part1(layers))

	image := part2(layers)
	fmt.Println("Part2:")
	printLayer(image)
}

func part1(layers []string) int {
	fewestZeroes := width * height
	var result int
	for _, layer := range layers {
		byteCount := make(map[byte]int)
		for i := 0; i < len(layer); i++ {
			byteCount[layer[i]]++
		}
		if byteCount['0'] < fewestZeroes {
			fewestZeroes = byteCount['0']
			result = byteCount['1'] * byteCount['2']
		}
	}
	return result
}

func part2(layers []string) string {
	image := make([]byte, width*height)
	for px := 0; px < width*height; px++ {
		for layer := 0; layer < len(layers); layer++ {
			if layers[layer][px] == black {
				image[px] = ' '
				break
			} else if layers[layer][px] == white {
				image[px] = white
				break
			}
		}
	}
	return string(image)
}

func printLayer(layer string) {
	for i := 0; i < height; i++ {
		fmt.Println(layer[i*width : (i+1)*width])
	}
}

func parseFile(path string) []string {
	data, _ := ioutil.ReadFile(path)
	s := string(data)
	layers := make([]string, len(s)/(width*height))
	for i := 0; i < len(layers); i++ {
		layers[i] = s[i*width*height : (i+1)*height*width]
	}
	return layers
}
