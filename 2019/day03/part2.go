package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type Point struct {
	X int
	Y int
}

func main() {
	paths, err := readLines("input.txt")
	if err != nil {
		fmt.Println(err.Error())
	}

	pathMap := make(map[Point]int)
	firstPath := paths[0]
	x := 0
	y := 0
	totalDist := 0
	for _, movement := range strings.Split(firstPath, ",") {
		direction := movement[:1]
		dist, err := strconv.Atoi(movement[1:])
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		for j := 0; j < dist; j++ {
			switch direction {
			case "R":
				x++
			case "U":
				y++
			case "L":
				x--
			case "D":
				y--
			}
			totalDist++
			if _, ok := pathMap[Point{x, y}]; !ok {
				pathMap[Point{x, y}] = totalDist
			}
		}
	}

	secondPath := paths[1]
	minSum := 99999999
	x = 0
	y = 0
	totalDist = 0
	for _, movement := range strings.Split(secondPath, ",") {
		direction := movement[:1]
		dist, err := strconv.Atoi(movement[1:])
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		for j := 0; j < dist; j++ {
			switch direction {
			case "R":
				x++
			case "U":
				y++
			case "L":
				x--
			case "D":
				y--
			}
			totalDist++
			point := Point{x, y}
			if pathDist, ok := pathMap[point]; ok {
				sum := totalDist + pathDist
				if sum < minSum {
					minSum = sum
				}
			}
		}
	}

	fmt.Printf("Answer: %d\n", minSum)
}

func manhattanDist(a, b Point) int {
	return int(math.Abs(float64(a.X-b.X)) + math.Abs(float64(a.Y-b.Y)))
}

// readLines creates a []string with each entry being a line from the file 'path'
func readLines(path string) ([]string, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	split := strings.Split(string(bytes), "\n")
	split = split[:len(split)-1]

	return split, nil
}
