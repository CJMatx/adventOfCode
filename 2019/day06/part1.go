package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type SpaceObject struct {
	name     string
	depth    int
	parent   *SpaceObject
	children []*SpaceObject
}

var santa, you *SpaceObject

func main() {
	orbits := parseFile("2019/day06/input.txt")

	totalOrbits, _ := buildOrbitMap("COM", 0, orbits)
	fmt.Println("Part1:", totalOrbits)

	// Travel to the parent of each before calculating shortestPath
	santa = santa.parent
	you = you.parent

	shortestPath := findShortestPath()
	fmt.Println("Part2:", shortestPath)
}

func findShortestPath() int {
	totalTransfers := 0
	if santa.depth < you.depth {
		you = you.parent
		totalTransfers++
	} else if you.depth < santa.depth {
		santa = santa.parent
		totalTransfers++
	} else if you.name == santa.name {
		return 0
	} else {
		you = you.parent
		santa = santa.parent
		totalTransfers += 2
	}
	return totalTransfers + findShortestPath()
}

func buildOrbitMap(currentName string, depth int, orbits map[string][]string) (int, *SpaceObject) {
	currentObj := &SpaceObject{name: currentName, depth: depth, children: []*SpaceObject{}}
	totalDepth := 0
	for _, childName := range orbits[currentName] {
		childDepth, child := buildOrbitMap(childName, depth+1, orbits)
		totalDepth += childDepth
		child.parent = currentObj
		currentObj.children = append(currentObj.children, child)
	}

	if currentName == "SAN" {
		santa = currentObj
	} else if currentName == "YOU" {
		you = currentObj
	}

	return totalDepth + depth, currentObj
}

func parseFile(path string) map[string][]string {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	orbits := make(map[string][]string)
	split := strings.Split(string(bytes), "\n")
	for _, line := range split {
		orbitSplit := strings.Split(line, ")")
		if len(orbitSplit) != 2 {
			continue
		}
		if children, ok := orbits[orbitSplit[0]]; !ok {
			orbits[orbitSplit[0]] = []string{orbitSplit[1]}
		} else {
			orbits[orbitSplit[0]] = append(children, orbitSplit[1])
		}

		if _, ok := orbits[orbitSplit[1]]; !ok {
			orbits[orbitSplit[1]] = []string{}
		}
	}

	return orbits
}
