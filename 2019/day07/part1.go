package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"strconv"
	"strings"
)

const (
	position  = 0
	immediate = 1

	add      = 1
	mul      = 2
	in       = 3
	out      = 4
	jmpTrue  = 5
	jmpFalse = 6
	less     = 7
	equals   = 8
	halt     = 99
)

type Computer struct {
	pointer     int
	inputParams []int
	outputParam int
	code        []int
}

func (c *Computer) add(val1, val2, outputPosition int) {
	c.code[outputPosition] = c.code[val1] + c.code[val2]
	c.pointer += 4
}

func (c *Computer) mul(val1, val2, outputPosition int) {
	c.code[outputPosition] = c.code[val1] * c.code[val2]
	c.pointer += 4
}

func (c *Computer) input(position int) {
	c.code[position] = c.inputParams[0]
	c.inputParams = c.inputParams[1:]
	c.pointer += 2
}

func (c *Computer) output(position int) {
	c.outputParam = c.code[position]
	c.pointer += 2
}

func (c *Computer) jumpIfTrue(val, position int) {
	if c.code[val] != 0 {
		c.pointer = c.code[position]
	} else {
		c.pointer += 3
	}
}

func (c *Computer) jumpIfFalse(val, position int) {
	if c.code[val] == 0 {
		c.pointer = c.code[position]
	} else {
		c.pointer += 3
	}
}

func (c *Computer) lessThan(val1, val2, position int) {
	if c.code[val1] < c.code[val2] {
		c.code[position] = 1
	} else {
		c.code[position] = 0
	}
	c.pointer += 4
}

func (c *Computer) equals(val1, val2, position int) {
	if c.code[val1] == c.code[val2] {
		c.code[position] = 1
	} else {
		c.code[position] = 0
	}
	c.pointer += 4
}

func (c *Computer) processInstruction() (op int, params []int) {
	instruction := c.code[c.pointer]
	op = instruction % 100
	if op == halt {
		return op, params
	}
	numParams := 3
	if op == in || op == out {
		numParams = 1
	} else if op == jmpTrue || op == jmpFalse {
		numParams = 2
	}
	params = make([]int, numParams)

	for i := 0; i < numParams; i++ {
		switch instruction / int(math.Pow10(i+2)) % 10 {
		case position:
			params[i] = c.code[c.pointer+i+1]
		case immediate:
			params[i] = c.pointer + i + 1
		}
	}
	return op, params
}

func (c *Computer) run() {
	running := true
	for running {
		op, params := c.processInstruction()
		switch op {
		case add:
			c.add(params[0], params[1], params[2])
		case mul:
			c.mul(params[0], params[1], params[2])
		case in:
			c.input(params[0])
		case out:
			c.output(params[0])
		case jmpTrue:
			c.jumpIfTrue(params[0], params[1])
		case jmpFalse:
			c.jumpIfFalse(params[0], params[1])
		case less:
			c.lessThan(params[0], params[1], params[2])
		case equals:
			c.equals(params[0], params[1], params[2])
		case halt:
			running = false
		default:
			log.Fatalf("Something went wrong! Unknown OpCode - op: %d, params: %v, pointer: %d", op, params, c.pointer)
		}
	}
}

func main() {
	codeAsString, err := readLine("input.txt")
	if err != nil {
		log.Fatal(err.Error())
	}

	// part1
	maxOutput := 0
	maxSequence := []int{0,0,0,0,0}
	for a := 0; a < 5; a++ {
		for b := 0; b < 5; b++ {
			if b == a {
				continue
			}
			for c := 0; c < 5; c++ {
				if c == b || c == a {
					continue
				}
				for d := 0; d < 5; d++ {
					if d == c || d == b || d == a {
						continue
					}
					for e := 0; e < 5; e++ {
						if e == d || e == c || e == b || e == a {
							continue
						}
						ampA := newComputer(codeAsString, []int{a, 0})
						ampA.run()
						ampB := newComputer(codeAsString, []int{b, ampA.outputParam})
						ampB.run()
						ampC := newComputer(codeAsString, []int{c, ampB.outputParam})
						ampC.run()
						ampD := newComputer(codeAsString, []int{d, ampC.outputParam})
						ampD.run()
						ampE := newComputer(codeAsString, []int{e, ampD.outputParam})
						ampE.run()
						if ampE.outputParam > maxOutput {
							maxOutput = ampE.outputParam
							maxSequence = []int{a,b,c,d,e}
						}
					}
				}
			}
		}
	}
	fmt.Println("Part1:", maxOutput, "Sequence:", maxSequence)
}

func newComputer(codeAsString string, input []int) Computer {
	splitCode := strings.Split(codeAsString, ",")
	computer := Computer{
		pointer:     0,
		code:        make([]int, len(splitCode)),
		inputParams: input,
	}

	for i, val := range splitCode {
		code, err := strconv.Atoi(val)
		if err != nil {
			log.Fatal(err.Error())
		}
		computer.code[i] = code
	}

	return computer
}

func readLine(path string) (string, error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	split := strings.Split(string(bytes), "\n")

	return split[0], nil
}
